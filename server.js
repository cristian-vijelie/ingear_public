// use "node server.js" to start the server

const express = require('express'),
    bodyParser = require('body-parser'),
    server = express(),
    fs = require('fs'),
    path = require('path'),
    root = __dirname;

function getTime() {
    let now = new Date();
    date = (now.getDate() < 10 ? '0' : '') + now.getDate() + '/' + 
        (now.getMonth() < 10 ? '0' : '') + now.getMonth() + '/' + 
        now.getFullYear() + ' ' + 
        (now.getHours() < 10 ? '0' : '') + now.getHours() + ':' + 
        (now.getMinutes() < 10 ? '0' : '') + now.getMinutes() + ':' + 
        (now.getSeconds() < 10 ? '0' : '') + now.getSeconds();
    return date;
}

console.log('Configuring static files...');
server.use(express.static(path.join(root, '/website')));
server.use(bodyParser.json());

console.log('Configuring endpoints...');

server.get('/about/texts', (request, response) => {
    let filePath = path.join(root, "/texts/about.json"),
        dataFromFile = fs.readFileSync(filePath);
    console.log('Received GET on /about/texts at ' + getTime() + " from " + request.headers['x-forwarded-for']);
    dataFromFile = JSON.parse(dataFromFile);
    response.send({
        status: "succesfull",
        data: dataFromFile
    });
});

server.get('/about/media/:domain', (request, response) => {
    let domain = request.params.domain;
    console.log("Received GET on /about/media/" + domain + ' at ' + getTime() + " from " + request.headers['x-forwarded-for']);
    let photoLinks = {},
        photosNr = 0;

    fs.readdirSync("website/assets/about/" + domain).forEach(file => {
        photoLinks[photosNr] = "assets/about/" + domain + "/" + file;
        photosNr++;
    });

    response.send({
        status: "succesfull",
        data: photoLinks
    });
});

server.get("/events", (request, response) => {
    console.log("Received GET on /events at " + getTime() + " from " + request.headers['x-forwarded-for']);
    let filePath = path.join(root, "/texts/events.json"),
        dataFromFile = fs.readFileSync(filePath);
    dataFromFile = JSON.parse(dataFromFile);
    response.send({
        status: "succesfull",
        data: dataFromFile
    });
});

server.get("/projects", (request, response) => {
    console.log("Received GET on /projects at " + getTime() + " from " + request.headers['x-forwarded-for']);
    let filePath = path.join(root, "/texts/projects.json"),
        dataFromFile = fs.readFileSync(filePath);
    dataFromFile = JSON.parse(dataFromFile);
    response.send({
        status: "succesfull",
        data: dataFromFile
    });
});

server.get("/research", (request, response) => {
    console.log("Received GET on /research at " + getTime() + " from " + request.headers['x-forwarded-for']);
    let filePath = path.join(root, "/texts/research.json"),
        dataFromFile = fs.readFileSync(filePath);
    dataFromFile = JSON.parse(dataFromFile);
    response.send({
        status: "succesfull",
        data: dataFromFile
    });
});

server.get("/team", (request, response) => {
    console.log("Received GET on /team at " + getTime() + " from " + request.headers['x-forwarded-for']);
    let filePath = path.join(root, "/texts/team.json"),
        dataFromFile = fs.readFileSync(filePath);
    dataFromFile = JSON.parse(dataFromFile);
    response.send({
        status: "succesfull",
        data: dataFromFile
    });
});

server.listen(3000, () =>
    console.log('Server is listening on http://localhost:3000/')
);