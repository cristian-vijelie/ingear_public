let homePage, currentPage, contentPart, nextPage, nextPagePosition, currentPagePosition, body,
    currentPageName = "homePage",
    homePagePosition = 1,
    canScroll = 1,
    isResponsive = (screen.width < 800 && screen.height < 600) || screen.width < screen.height;
contentLoaded = [{
        "page": "aboutPage",
        "status": "notLoaded"
    },
    {
        "page": "eventsPage",
        "status": "notLoaded"
    },
    {
        "page": "projectsPage",
        "status": "notLoaded"
    },
    {
        "page": "researchPage",
        "status": "notLoaded"
    },
    {
        "page": "teamPage",
        "status": "notLoaded"
    },
    {
        "page": "sponsorsPage",
        "status": "loaded"
    },
    {
        "page": "contactPage",
        "status": "notLoaded"
    }
];
if (screen.width >= 800 && screen.height <= 1024) {
    aboutMaxPhotos = 2;
    maxProjects = 3;
    maxArticles = 3;
    maxMembers = 4;
} else {
    aboutMaxPhotos = 1;
    maxProjects = 1;
    maxArticles = 1;
}

const html = document.getElementsByTagName("html")[0],
    pagePositions = [{
            "name": "aboutPage",
            "pos": 1
        },
        {
            "name": "eventsPage",
            "pos": 2
        },
        {
            "name": "projectsPage",
            "pos": 3
        },
        {
            "name": "researchPage",
            "pos": 4
        },
        {
            "name": "teamPage",
            "pos": 5
        },
        {
            "name": "sponsorsPage",
            "pos": 6
        },
        {
            "name": "contactPage",
            "pos": 7
        }
    ],
    aboutPhotos = [{
            "domain": "robotics",
            "url": "/about/media/robotics"
        },
        {
            "domain": "drones",
            "url": "/about/media/drones"
        },
        {
            "domain": "pcb",
            "url": "/about/media/pcb"
        },
        {
            "domain": "medical",
            "url": "/about/media/medical"
        },
        {
            "domain": "iot",
            "url": "/about/media/iot"
        }
    ];

window.onbeforeunload = () => {
    window.scroll(0, 0);
};

document.addEventListener("DOMContentLoaded", () => {
    body = document.querySelector("body");
    homePage = document.getElementById("homePage");
    contentPart = document.getElementById("contentPart");
    currentPage = homePage;
    currentPageName = "homePage";

    let clickable = document.getElementsByClassName('link');
    for (i = 0; i < clickable.length; i++)
        clickable[i].addEventListener('click', clickEventHandler);

    clickable = document.getElementsByClassName('whiteLink');
    for (i = 0; i < clickable.length; i++)
        clickable[i].addEventListener('click', clickEventHandler);

    clickable = document.getElementsByClassName('menuImage');
    for (i = 0; i < clickable.length; i++)
        clickable[i].addEventListener('click', clickEventHandler);

    document.getElementById("seeText").addEventListener("click", clickEventHandler);

    if (!isResponsive) {
        document.addEventListener("wheel", scrollEventHandler);
        document.addEventListener("keydown", keyEventHandler, false);
    } else {
        for (let i in contentLoaded) {
            requestContent(contentLoaded[i].page);
            contentLoaded[i].status = "loaded";
        }
    }

    //responsive

    let domainsHeight = document.getElementById('aboutDomains'),
        eventDirection = document.getElementById('eventsContainer');
    if (screen.width > screen.height) {
        domainsHeight.style.height = 16 + "vh";
        domainsHeight.style.flexWrap = "none";
        eventDirection.style.flexDirection = "row";
    }
});

function requestContent(page) {
    if (page === "aboutPage") {
        loadAbout();
    } else if (page === "eventsPage") {
        loadEvents();
    } else if (page === "projectsPage") {
        loadProjects();
    } else if (page === "researchPage") {
        loadResearch();
    } else if (page === "teamPage") {
        loadTeam();
    }
}

function clickEventHandler(event) {
    let nextPagePosition;
    nextPageName = event.target.dataset.url;
    nextPage = document.getElementById(nextPageName);
    if (currentPage === homePage) {
        if (isResponsive) {
            nextPage.scrollIntoView({
                behavior: 'smooth',
                block: "start"
            });
        } else {
            for (let i in pagePositions) {
                if (pagePositions[i].name === nextPageName) {
                    nextPagePosition = pagePositions[i].pos;
                }
            }
            if (nextPagePosition != homePagePosition) {
                homePage.classList.remove("moveDown");
                homePagePosition = nextPagePosition;
                homePage.classList.remove("hasTransition");
                html.style.setProperty("--main-page-down", (nextPagePosition - 1) * 100 + "vh");
                homePage.classList.add("moveDown");
                homePage.scrollIntoView();
            }
            for (let i in contentLoaded) {
                if (contentLoaded[i].page === nextPageName) {
                    if (contentLoaded[i].status === "notLoaded") {
                        requestContent(nextPageName);
                        contentLoaded[i].status = "loaded";
                    }
                    break;
                }
            }
            homePage.classList.remove("moveDown");
            html.style.setProperty("--main-page-down", (nextPagePosition - 1) * 100 + "vh");
            homePage.classList.add("hasTransition", "moveDownLeft");
            contentPart.classList.add("hasTransition", "moveLeft");
        }
        currentPage = nextPage;
        currentPageName = nextPageName;
        currentPagePosition = nextPagePosition;
    } else {
        if (currentPageName === "researchPage") {
            researchTimer.stop();
        }
        if (nextPage === homePage) {
            if (isResponsive) {
                nextPage.scrollIntoView({
                    behavior: 'smooth',
                    block: "start"
                });
            } else {
                contentPart.classList.remove("moveLeft");
                homePage.classList.remove("moveDownLeft");
                html.style.setProperty("--main-page-down", (homePagePosition - 1) * 100 + "vh");
                homePage.classList.add("moveDown");
                setTimeout(() => {
                    homePage.classList.remove("hasTransition");
                    html.style.setProperty("--main-page-down", "0vh");
                    homePagePosition = 1;
                    window.scroll(0, 0);
                }, 300);
                homePage.classList.add("hasTransition");
            }
            body.className = "";
            currentPage = homePage;
            currentPageName = "homePage";
        } else {
            if (isResponsive) {
                nextPage.scrollIntoView({
                    behavior: 'smooth',
                    block: "start"
                });
            } else {
                for (let i in pagePositions) {
                    if (pagePositions[i].name === nextPageName)
                        nextPagePosition = pagePositions[i].pos;
                    if (pagePositions[i].name === currentPageName)
                        currentPagePosition = pagePositions[i].pos;
                }
                for (let i in contentLoaded) {
                    if (contentLoaded[i].page === nextPageName) {
                        if (contentLoaded[i].status === "notLoaded") {
                            requestContent(nextPageName);
                            if (nextPageName === "researchPage") {
                                researchTimer.start();
                            }
                            contentLoaded[i].status = "loaded";
                        }
                        break;
                    }
                }
                html.style.setProperty("--main-page-down", (nextPagePosition - 1) * 100 + "vh");
                homePagePosition = nextPagePosition;
                currentPage = nextPage;
                currentPageName = nextPageName;
                currentPagePosition = nextPagePosition;
                homePagePosition = nextPagePosition;
                nextPage.scrollIntoView({
                    behavior: 'smooth'
                });
            }
        }
    }
}

function scrollUp(key) {
    if (currentPageName !== "aboutPage" && currentPageName !== "homePage" && (key === "ArrowUp" || key === undefined)) {
        for (let i in pagePositions) {
            if (pagePositions[i].name === currentPageName) {
                currentPagePosition = pagePositions[i].pos;
                nextPagePosition = currentPagePosition - 1;
            }
        }
        for (let i in pagePositions) {
            if (pagePositions[i].pos === nextPagePosition)
                nextPageName = pagePositions[i].name;
        }
        for (let i in contentLoaded) {
            if (contentLoaded[i].page === nextPageName) {
                if (contentLoaded[i].status === "notLoaded") {
                    requestContent(nextPageName);
                    contentLoaded[i].status = "loaded";
                }
                break;
            }
        }
        if (nextPageName === "researchPage") {
            researchTimer.start();
        }
        html.style.setProperty("--main-page-down", (nextPagePosition - 1) * 100 + "vh");
        homePagePosition = nextPagePosition;
        nextPage = document.getElementById(nextPageName);
        nextPage.scrollIntoView({
            behavior: 'smooth'
        });
        currentPage = nextPage;
        currentPageName = nextPageName;
        currentPagePosition = nextPagePosition;
    } else {
        if (currentPageName === "aboutPage" && homePagePosition === 1) {
            contentPart.classList.remove("moveLeft");
            html.style.setProperty("--main-page-down", "0vh");
            homePage.classList.replace("moveDownLeft", "moveDown");
            currentPage = homePage;
            currentPageName = "homePage";
            currentPagePosition = 1;
        }
    }
}

function scrollDown(key) {
    if (currentPageName === "homePage" && homePagePosition === 1) {
        for (let i in contentLoaded) {
            if (contentLoaded[i].page === "aboutPage") {
                if (contentLoaded[i].status === "notLoaded") {
                    requestContent("aboutPage");
                    contentLoaded[i].status = "loaded";
                }
                break;
            }
        }
        contentPart.classList.add("moveLeft");
        contentPart.classList.add("hasTransition");
        html.style.setProperty("--main-page-down", "0vh");
        homePage.classList.remove("moveDown");
        homePage.classList.add("moveDownLeft", "hasTransition");
        currentPage = document.getElementById("aboutPage");
        currentPageName = "aboutPage";
        currentPagePosition = 1;
    } else {
        if (currentPageName !== "contactPage" && currentPageName !== "homePage" && (key === "ArrowDown" || key === undefined)) {
            for (let i in pagePositions) {
                if (pagePositions[i].name === currentPageName)
                    nextPagePosition = pagePositions[i].pos + 1;
            }
            for (let i in pagePositions) {
                if (pagePositions[i].pos === nextPagePosition)
                    nextPageName = pagePositions[i].name;
            }
            for (let i in contentLoaded) {
                if (contentLoaded[i].page === nextPageName) {
                    if (contentLoaded[i].status === "notLoaded") {
                        requestContent(nextPageName);
                        contentLoaded[i].status = "loaded";
                    }
                    break;
                }
            }
            if (nextPageName === "researchPage") {
                researchTimer.start();
            }
            html.style.setProperty("--main-page-down", (nextPagePosition - 1) * 100 + "vh");
            homePagePosition = nextPagePosition;
            nextPage = document.getElementById(nextPageName);
            nextPage.scrollIntoView({
                behavior: 'smooth'
            });
            currentPage = nextPage;
            currentPageName = nextPageName;
            currentPagePosition = nextPagePosition;
        }
    }
}

function keyEventHandler(event) {
    let key = event.key;
    if (aboutGalleryIsActive === 1) {
        hidePhotos();
    }
    if (currentPageName === "researchPage") {
        researchTimer.stop();
    }
    if (canScroll === 1) {
        if (key === "ArrowUp" || key === "ArrowLeft") {
            scrollUp(key);
        } else if (key === "ArrowDown" || key === "ArrowRight") {
            scrollDown(key);
        }
        canScroll = 0;
        setTimeout(() => {
            canScroll = 1;
        }, 500);
    }
}

function scrollEventHandler(event) {
    event = window.event || event;
    let delta = Math.max(-1, Math.min(1, (event.deltaY || -event.detail)));
    if (aboutGalleryIsActive === 1) {
        hidePhotos();
    }
    if (currentPageName === "researchPage") {
        researchTimer.stop();
    }
    if (canScroll === 1) {
        if (delta === -1) {
            scrollUp();
        } else {
            scrollDown();
        }
        canScroll = 0;
        setTimeout(() => {
            canScroll = 1;
        }, 300);
    }
}

function myFunction() { //the hell is this?
    document.getElementById("myDropdown").classList.toggle("show");
}

window.onclick = (event) => {
    if (!event.target.matches('.menuIcon')) {
        let dropdowns = document.getElementsByClassName("dropdown-content"),
            i;
        for (i = 0; i < dropdowns.length; i++) {
            let openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
            }
        }
    }
};

function homeFunction() {
    document.getElementById('homeDropdown').classList.toggle('show');
}

function aboutFunction() {
    document.getElementById('aboutDropdown').classList.toggle('show');
}

function projectsFunction() {
    document.getElementById('projectsDropdown').classList.toggle('show');
}

function eventsFunction() {
    document.getElementById('eventsDropdown').classList.toggle('show');
}

function researchFunction() {
    document.getElementById('researchDropdown').classList.toggle('show');
}

function teamFunction() {
    document.getElementById('teamDropdown').classList.toggle('show');
}

function sponsorsFunction() {
    document.getElementById('sponsorsDropdown').classList.toggle('show');
}

function contactFunction() {
    document.getElementById('contactDropdown').classList.toggle('show');
}

window.onclick = (event) => {
    if (!event.target.matches('.menuIcon')) {
        let dropdowns = document.getElementsByClassName('dropdown-content');
        for (let i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
            }
        }
    }
};