let rightmostMemberTop, rightmostMemberBottom, leftmostMemberTop, leftmostMemberBottom, maxMembers;

function createMemberBox(doc, member) {
    let newMemberBox = doc.createElement("span"),
        newImage = doc.createElement("img"),
        newTextBox = doc.createElement("span"),
        newName = doc.createElement("p"),
        newRole = doc.createElement("p"),
        newLinkedinLink = doc.createElement("a"),
        newLinkedinLogo = doc.createElement("img"),
        newMail = doc.createElement("a"),
        newMailLogo = doc.createElement("img");

    newMemberBox.classList.add("teamBox", "visibleMember");
    newImage.className = "teamImage";
    newTextBox.className = "teamTextBox";
    newName.className = "teamName";
    newRole.className = "teamRole";
    newLinkedinLogo.className = "linkedinLogo";
    newMailLogo.className = "mailLogo";

    newImage.src = member.photo;
    newName.innerText = member.name;
    newRole.innerText = member.role;
    newLinkedinLink.target = "_blank";
    newLinkedinLink.rel = "noopener noreferrer";
    newLinkedinLink.href = member.linkedin;
    newLinkedinLogo.src = "assets/links/linkedinLogo.png";
    newMail.rel = "noopener noreferrer";
    newMail.href = "mailto:" + member.mail;
    newMailLogo.src = "assets/links/mailLogo.png";

    newTextBox.appendChild(newName);
    newTextBox.appendChild(newRole);
    newLinkedinLink.appendChild(newLinkedinLogo);
    newMail.appendChild(newMailLogo);
    newMemberBox.appendChild(newImage);
    newMemberBox.appendChild(newTextBox);
    newMemberBox.appendChild(newLinkedinLink);
    newMemberBox.appendChild(newMail);

    return newMemberBox;
}
document.addEventListener("DOMContentLoaded", () => {
    let clickable = document.getElementById("nextMembers");
    clickable.addEventListener('click', slideMembers);

    clickable = document.getElementById("prevMembers");
    clickable.addEventListener('click', slideMembers);
});

function loadTeam() {
    fetch("/team")
            .then(response => response.json())
            .then(json => json.data)
            .then((data) => {
                const doc = document;
                let membersNr = data.members.length,
                    members = [],
                    teamContainers = doc.getElementsByClassName("peopleBox"),
                    nextMembers = document.getElementById("nextMembers"),
                    prevMembers = document.getElementById("prevMembers"),
                    teamHeight = document.getElementById("teamPage"),
                    secondHalf = document.getElementById("teamSecondHalf");

                if (screen.width > screen.height) {
                    maxMembers = 3;
                } else {
                    maxMembers = membersNr;
                    teamHeight.style.height = 50 + membersNr * 45 + "vh";
                    secondHalf.style.height = membersNr * 40 + "vh";
                    if (membersNr % 2 == 0) {
                        teamContainers[0].style.height = membersNr / 2 * 40 + "vh";
                        teamContainers[1].style.height = membersNr / 2 * 40 + "vh";
                    } else {
                        teamContainers[0].style.height = (membersNr / 2 + 1) * 40 + "vh";
                        teamContainers[1].style.height = membersNr / 2 * 40 + "vh";
                    }
                }
                for (let i = 0; i < membersNr; i++) {
                    members[i] = createMemberBox(doc, data.members[i]);
                }

                if ((membersNr > maxMembers * 2) && (screen.width > screen.height)) {
                    for (let i = maxMembers * 2; i < membersNr; i++) {
                        members[i].classList.replace("visibleMember", "remove");
                    }
                } else {
                    nextMembers.classList.add("makeInvisible");
                }

                prevMembers.classList.add("makeInvisible");

                for (let i = 0; i < membersNr; i++) {
                    teamContainers[i % 2].appendChild(members[i]);
                }
                leftmostMemberTop = 0;
                rightmostMemberTop = Math.round(membersNr / 2) > maxMembers ? 2 : Math.round(membersNr / 2) - 1;
                leftmostMemberBottom = Math.round(membersNr / 2);
                rightmostMemberBottom = rightmostMemberTop + membersNr -Math.round(membersNr / 2);
            })
            .catch("Could not fetch /team");
}

function slideMembers(event) {
    let members = document.getElementsByClassName("teamBox"),
        membersNr = members.length,
        membersNrTop = Math.round(membersNr / 2),
        membersNrBottom = membersNr - membersNrTop,
        nextMembers = document.getElementById("nextMembers"),
        prevMembers = document.getElementById("prevMembers");
    if (event.target.id === "nextMembers" && rightmostMemberBottom < membersNr - 1) {
        slideNrTop = (membersNrTop - rightmostMemberTop - 1) / maxMembers >= 1 ? maxMembers : (membersNrTop - rightmostMemberTop - 1) % maxMembers;
        if (membersNr % 2 === 1) {
            slideNrBottom = slideNrTop - 1;
        } else {
            slideNrBottom = slideNrTop;
        }
        for (let i = 0; i < slideNrTop; i++) {
            members[leftmostMemberTop].classList.replace("visibleMember", "remove");
            leftmostMemberTop++;
            rightmostMemberTop++;
            members[rightmostMemberTop].classList.replace("remove", "visibleMember");
        }
        for (let i = 0; i < slideNrBottom; i++) {
            members[leftmostMemberBottom].classList.replace("visibleMember", "remove");
            leftmostMemberBottom++;
            rightmostMemberBottom++;
            members[rightmostMemberBottom].classList.replace("remove", "visibleMember");
        }
    } else if (event.target.id === "prevMembers" && leftmostMemberTop > 0) {
        slideNrTop = leftmostMemberTop / maxMembers >= 1 ? maxMembers : leftmostMemberTop % maxMembers;
        if (membersNr % 2 === 1) {
            slideNrBottom = slideNrTop - 1;
        } else {
            slideNrBottom = slideNrTop;
        }

        for (let i = 0; i < slideNrTop; i++) {
            members[rightmostMemberTop].classList.replace("visibleMember", "remove");
            leftmostMemberTop--;
            rightmostMemberTop--;
            members[leftmostMemberTop].classList.replace("remove", "visibleMember");
        }
        for (let i = 0; i < slideNrBottom; i++) {
            members[rightmostMemberBottom].classList.replace("visibleMember", "remove");
            leftmostMemberBottom--;
            rightmostMemberBottom--;
            members[leftmostMemberBottom].classList.replace("remove", "visibleMember");
        }
    }
    if (rightmostMemberBottom === membersNr - 1 && nextMembers.classList.contains("makeInvisible") === false) {
        nextMembers.classList.add("makeInvisible");
    } else if (nextMembers.classList.contains("makeInvisible")) {
        nextMembers.classList.remove("makeInvisible");
    }
    if (leftmostMemberTop === 0 && prevMembers.classList.contains("makeInvisible") === false) {
        prevMembers.classList.add("makeInvisible");
    } else if (prevMembers.classList.contains("makeInvisible")) {
        prevMembers.classList.remove("makeInvisible");
    }
}