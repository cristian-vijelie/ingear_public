let rightmostEvent, leftmostEvent, maxEvents, eventsHeight = 0;

function createEventBox(eventsContainer, event) {
    let newEventContainer = document.createElement("span");

    newEventContainer.classList.add("eventContainer", "visibleEvent");

    if (event.style === "light")
        newEventContainer.style.backgroundImage = "url(\"assets/events/eventsCard1.png\")";
    else {
        newEventContainer.style.backgroundImage = "url(\"assets/events/eventsCard2.png\")";
        newEventContainer.style.color = "white";
    }

    let newEventText = document.createElement("span");
    newEventText.classList.add("eventText");

    let newEventTitle = document.createElement("p");
    newEventTitle.classList.add("eventTitle");
    newEventTitle.innerText = event.title;

    let newEventContent = document.createElement("p");
    newEventContent.classList.add("eventContent");
    newEventContent.innerText = event.date + "\n" + event.location + "\n" + event.powered;


    let newEventLink = document.createElement("a");
    if (event.style === "light")
        newEventLink.classList.add("link");
    else
        newEventLink.classList.add("whiteLink");
    newEventLink.innerText = "See Event >";

    newEventText.appendChild(newEventTitle);
    newEventText.appendChild(newEventContent);
    newEventText.appendChild(newEventLink);

    newEventContainer.appendChild(newEventText);

    let newEventPhoto = document.createElement("img");
    newEventPhoto.classList.add("eventPhoto");
    newEventPhoto.src = event.photo;

    newEventContainer.appendChild(newEventPhoto);

    eventsContainer.appendChild(newEventContainer);
}

document.addEventListener("DOMContentLoaded", () => {
    let clickable = document.getElementById("nextEvents");
    clickable.addEventListener('click', slideEvents);

    clickable = document.getElementById("prevEvents");
    clickable.addEventListener('click', slideEvents);
});

function loadEvents() {
    fetch("/events")
            .then(response => response.json())
            .then(json => json.data.events)
            .then((eventData) => {
                eventsContainer = document.getElementById("eventsContainer");
                eventsHeight = document.getElementById("eventsPage");
                let eventsCards = document.getElementById("events");
                let eventsNumber = eventData.length;

                if (screen.width > screen.height) {
                    maxEvents = 2;
                } else {
                    maxEvents = eventsNumber;
                    eventsHeight.style.height = 20 + eventsNumber * 45 + "vh";
                    eventsCards.style.height = eventsNumber * 40 + "vh";
                    eventsContainer.style.height = eventsNumber * 40 + "vh";
                }

                for (let i in eventData) {
                    createEventBox(eventsContainer, eventData[i]);
                }

                let events = document.getElementsByClassName("eventContainer"),
                    nextEvents = document.getElementById("nextEvents");
                if (screen.width > screen.height) {
                    for (let i = 0; i < eventsNumber; i++)
                        events[i].style.width = 40 + "vw";
                }
                if (eventsNumber > maxEvents) {
                    for (let i = maxEvents; i < eventsNumber; i++) {
                        events[i].classList.replace("visibleEvent", "remove");
                    }
                } else {
                    nextEvents.classList.add("makeInvisible");
                }
                rightmostEvent = eventsNumber < 2 ? 0 : 1;
                leftmostEvent = 0;
                document.getElementById("prevEvents").classList.add("makeInvisible");
            })
            .catch("Could not fetch /events");
}

function slideEvents(event) {
    let events = document.getElementsByClassName("eventContainer"),
        eventsNr = events.length,
        eventNext = document.getElementById("nextEvents"),
        eventPrev = document.getElementById("prevEvents");
    if (event.target.id === "nextEvents" && rightmostEvent < eventsNr - 1) {
        slideNr = (eventsNr - rightmostEvent - 1) / maxEvents >= 1 ? maxEvents : (eventsNr - rightmostEvent - 1) % maxEvents;
        for (let i = 0; i < slideNr; i++) {
            events[leftmostEvent].classList.replace("visibleEvent", "remove");
            leftmostEvent++;
            rightmostEvent++;
            events[rightmostEvent].classList.replace("remove", "visibleEvent");
        }
    } else if (event.target.id === "prevEvents" && leftmostEvent > 0) {
        slideNr = leftmostEvent / maxEvents >= 1 ? maxEvents : leftmostEvent % maxEvents;
        for (let i = 0; i < slideNr; i++) {
            events[rightmostEvent].classList.replace("visibleEvent", "remove");
            leftmostEvent--;
            rightmostEvent--;
            events[leftmostEvent].classList.replace("remove", "visibleEvent");
        }
    }
    if (rightmostEvent === eventsNr - 1 && eventNext.classList.contains("makeInvisible") === false) {
        eventNext.classList.add("makeInvisible");
    } else if (eventNext.classList.contains("makeInvisible")) {
        eventNext.classList.remove("makeInvisible");
    }
    if (leftmostEvent === 0 && eventPrev.classList.contains("makeInvisible") === false) {
        eventPrev.classList.add("makeInvisible");
    } else if (eventPrev.classList.contains("makeInvisible")) {
        eventPrev.classList.remove("makeInvisible");
    }
}