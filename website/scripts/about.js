let aboutRightmostImage, aboutLastImages, aboutLeftmostImage, aboutMaxPhotos,
    aboutPhotosMov = 0,
    aboutGalleryIsActive = 0,
    aboutFirstTime = 0;

document.addEventListener("DOMContentLoaded", () => {
    let clickable = document.getElementsByClassName("aboutDomain");
    for (i = 0; i < clickable.length; i++)
        clickable[i].addEventListener('click', showPhotos);

    clickable = document.getElementById("aboutGalleryLeftArrow");
    clickable.addEventListener("click", slidePhotos);

    clickable = document.getElementById("aboutGalleryRightArrow");
    clickable.addEventListener("click", slidePhotos);

    clickable = document.getElementById("aboutGalleryBackground");
    clickable.addEventListener('click', hidePhotos);
});

function showPhotos(event) {
    gallery = document.getElementById("aboutGallery");
    galleryBackground = document.getElementById("aboutGalleryBackground");
    if (aboutLastImages !== event.target.dataset.link) {
        aboutLastImages = event.target.dataset.link;
        aboutLeftmostImage = 0;
        aboutRightmostImage = 1;

        let galleryLeftArrow = document.getElementById("aboutGalleryLeftArrow"),
            galleryRightArrow = document.getElementById("aboutGalleryRightArrow"),
            gallerySlideshow = document.getElementById("aboutGallerySlideshow");

        fetch("/about/media/" + event.target.dataset.link)
            .then(response => response.json())
            .then(json => json.data)
            .then((photosPaths) => {
                let photosNumber = 0;
                gallerySlideshow.innerHTML = '';
                for (let i in photosPaths) {
                    let newSpan = document.createElement("span");
                    newSpan.classList.add("aboutGalleryPhoto");
                    let newPhoto = document.createElement("img");
                    newPhoto.src = photosPaths[i];
                    newSpan.appendChild(newPhoto);
                    gallerySlideshow.appendChild(newSpan);
                    photosNumber++;
                }
                let galleryPhotos = document.getElementsByClassName("aboutGalleryPhoto");
                if (photosNumber > aboutMaxPhotos) {
                    for (let i = aboutMaxPhotos; i < photosNumber; i++)
                        galleryPhotos[i].classList.add("remove");
                    galleryRightArrow.classList.remove("makeInvisible");
                } else {
                    galleryRightArrow.classList.add("makeInvisible");
                }
                galleryLeftArrow.classList.add("makeInvisible");
            })
            .catch("Could not fetch from /about/media/" + event.target.dataset.link);
    }
    gallery.classList.replace("makeInvisible", "makeVisible");
    gallery.classList.replace("fadeOut", "fadeIn");

    galleryBackground.classList.replace("makeInvisible", "makeVisible");
    galleryBackground.classList.replace("fadeOut", "fadeIn");
}

function slidePhotos(event) {
    let galleryPhotos = document.getElementsByClassName("aboutGalleryPhoto"),
        imagesNr = galleryPhotos.length,
        galleryLeftArrow = document.getElementById("aboutGalleryLeftArrow"),
        galleryRightArrow = document.getElementById("aboutGalleryRightArrow");
    if (event.target.id === "aboutGalleryRightArrow" && aboutRightmostImage < imagesNr - 1) {
        slideNr = (imagesNr - aboutRightmostImage - 1) / aboutMaxPhotos >= 1 ? aboutMaxPhotos : (imagesNr - aboutRightmostImage - 1) % aboutMaxPhotos;
        for (let i = 0; i < slideNr; i++) {
            galleryPhotos[aboutLeftmostImage].classList.add("remove");
            aboutLeftmostImage++;
            aboutRightmostImage++;
            galleryPhotos[aboutRightmostImage].classList.remove("remove");
        }
    } else if (event.target.id === "aboutGalleryLeftArrow" && aboutLeftmostImage > 0) {
        slideNr = aboutLeftmostImage / aboutMaxPhotos >= 1 ? aboutMaxPhotos : aboutLeftmostImage % aboutMaxPhotos;
        for (let i = 0; i < slideNr; i++) {
            galleryPhotos[aboutRightmostImage].classList.add("remove");
            aboutLeftmostImage--;
            aboutRightmostImage--;
            galleryPhotos[aboutLeftmostImage].classList.remove("remove");
        }
    }
    if (aboutRightmostImage === imagesNr - 1 && galleryRightArrow.classList.contains("makeInvisible") === false) {
        galleryRightArrow.classList.add("makeInvisible");
    } else if (galleryRightArrow.classList.contains("makeInvisible")) {
        galleryRightArrow.classList.remove("makeInvisible");
    }
    if (aboutLeftmostImage === 0 && galleryLeftArrow.classList.contains("makeInvisible") === false) {
        galleryLeftArrow.classList.add("makeInvisible");
    } else if (galleryLeftArrow.classList.contains("makeInvisible")) {
        galleryLeftArrow.classList.remove("makeInvisible");
    }
}

function hidePhotos() {
    let gallery = document.getElementById("aboutGallery"),
        galleryBackground = document.getElementById("aboutGalleryBackground");
    gallery.classList.replace("fadeIn", "fadeOut");
    galleryBackground.classList.replace("fadeIn", "fadeOut");
    setTimeout(() => {
        gallery.classList.replace("makeVisible", "makeInvisible");
        galleryBackground.classList.replace("makeVisible", "makeInvisible");
    }, 400);
    aboutGalleryIsActive = 0;
}

function loadAbout() {
    fetch("/about/texts")
        .then(response => response.json())
        .then(json => json.data.text)
        .then((text) => {
            const doc = document;
            doc.getElementById("aboutWho").innerHTML = text.who;
            doc.getElementById("aboutWhat").innerHTML = text.what;
            doc.getElementById("aboutAlso").innerHTML = text.also;
        })
        .catch("Could not fetch /about/texts");
}