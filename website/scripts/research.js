let rightmostArticle, leftmostArticle, maxArticles, researchTimer, articleSlideTime = 7000;

function Timer(fn, t) {
    let timerObj = setInterval(fn, t);
    this.stop = function () {
        if (timerObj) {
            clearInterval(timerObj);
            timerObj = null;
        }
        return this;
    };
    this.start = function () {
        if (!timerObj) {
            this.stop();
            timerObj = setInterval(fn, t);
        }
        return this;
    };
    this.reset = function (newT) {
        t = newT;
        return this.stop().start();
    };
}

researchTimer = new Timer(slideArticles, articleSlideTime);
researchTimer.stop();

function createArticleBox(doc, article) {
    let newArticleBox = doc.createElement("span"),
        newArticleHeader = doc.createElement("div"),
        newArticleTitle = doc.createElement("span"),
        newArticlePhotoContainer = doc.createElement("span"),
        newArticlePhoto = doc.createElement("img"),
        newArticleContent = doc.createElement("p"),
        newArticleMore = doc.createElement("div"),
        newArticleLink = doc.createElement("a");

    newArticleBox.classList.add("articleBox", "visibleArticle");
    newArticleHeader.classList.add("articleHeader");
    newArticleTitle.classList.add("articleTitle");
    newArticlePhotoContainer.classList.add("articlePhoto");
    newArticleContent.classList.add("articleContent");
    newArticleMore.classList.add("articleMore");
    newArticleLink.classList.add("link");

    newArticleTitle.innerText = article.title;
    newArticleContent.innerText = article.content;
    newArticlePhoto.src = article.photoLink;
    newArticleLink.innerText = "Read more >";
    newArticleLink.href = article.moreLink;
    newArticleLink.target = "_blank";
    newArticleLink.rel = "noopener noreferrer";

    newArticleHeader.appendChild(newArticleTitle);
    newArticlePhotoContainer.appendChild(newArticlePhoto);
    newArticleHeader.appendChild(newArticlePhotoContainer);
    newArticleMore.appendChild(newArticleLink);
    newArticleBox.appendChild(newArticleHeader);
    newArticleBox.appendChild(newArticleContent);
    newArticleBox.appendChild(newArticleMore);

    return newArticleBox;
}

function loadResearch() {
    fetch("/research")
            .then(response => response.json())
            .then(json => json.data)
            .then((data) => {
                const doc = document;
                let articlesNr = data.articles.length,
                    articlesContainer1 = doc.getElementById("researchArticles1"),
                    articlesContainer2 = doc.getElementById("researchArticles2"),
                    articles = [];

                for (let i = 0; i < articlesNr; i++) {
                    articles[i] = createArticleBox(doc, data.articles[i]);
                }

                if (articlesNr > maxArticles) {
                    for (let i = maxProjects; i < articlesNr; i++) {
                        articles[i].classList.replace("visibleArticle", "remove");
                    }
                }

                for (i = 0; i < 2; i++) {
                    articlesContainer1.appendChild(articles[i]);
                }

                for (i = 2; i < articlesNr; i++) {
                    articlesContainer2.appendChild(articles[i]);
                }
                rightmostArticle = articlesNr < 3 ? articlesNr - 1 : 2;
                leftmostArticle = 0;
                researchTimer = new Timer(slideArticles, articleSlideTime);
                for (i = 0; i < articlesNr; i++) {
                    articles[i].addEventListener("mouseenter", () => {
                        researchTimer.stop();
                    });
                    articles[i].addEventListener("mouseout", () => {
                        researchTimer.start();
                    });
                }
            })
            .catch("Could not fetch /research");
}

function slideArticles() {
    let articleContainer1 = document.getElementById("researchArticles1"),
        articleContainer2 = document.getElementById("researchArticles2");

    articleContainer1.childNodes[0].classList.remove("visibleArticle");
    articleContainer1.childNodes[0].classList.add("remove");

    articleContainer2.childNodes[1].classList.add("visibleArticle");
    articleContainer2.childNodes[1].classList.remove("remove");

    articleContainer2.appendChild(articleContainer1.childNodes[0]);
    articleContainer1.appendChild(articleContainer2.childNodes[0]);
}