let rightmostProject, leftmostProject, maxProjects;

function createProjectBox(doc, project, projectsContainer) {
    let newProjectBox = doc.createElement("span"),
        newProjectPhoto = doc.createElement("div"),
        newProjectText = doc.createElement("span"),
        newProjectTitle = doc.createElement("p"),
        newProjectContent = doc.createElement("p"),
        newProjectFbLink = doc.createElement("a"),
        newProjectExtLink = doc.createElement("a"),
        newProjectFbLogo = doc.createElement("img"),
        newProjectLinkLogo = doc.createElement("img");

    newProjectBox.classList.add("projectBox", "visibleProject");
    newProjectPhoto.classList.add("imageBox");
    newProjectText.classList.add("prTextBox");
    newProjectTitle.classList.add("prTitleBox");
    newProjectContent.classList.add("prContentBox");
    newProjectFbLogo.classList.add("fbLogo");
    newProjectLinkLogo.classList.add("linkLogo");

    newProjectTitle.innerText = project.title;
    newProjectContent.innerText = project.description;
    newProjectFbLink.target = "_blank";
    newProjectFbLink.rel = "noopener noreferrer";
    newProjectFbLink.href = project.fbLink;
    newProjectFbLogo.src = "assets/links/fb-logo.png";
    newProjectFbLogo.alt = " ";
    newProjectExtLink.target = "_blank";
    newProjectExtLink.rel = "noopener noreferrer";
    newProjectExtLink.href = project.externalLink;
    newProjectLinkLogo.src = "assets/links/link.png";
    newProjectLinkLogo.alt = " ";

    newProjectText.appendChild(newProjectTitle);
    newProjectText.appendChild(newProjectContent);
    newProjectFbLink.appendChild(newProjectFbLogo);
    newProjectExtLink.appendChild(newProjectLinkLogo);

    newProjectBox.appendChild(newProjectPhoto);
    newProjectBox.appendChild(newProjectText);
    newProjectBox.appendChild(newProjectFbLink);
    newProjectBox.appendChild(newProjectExtLink);

    projectsContainer.appendChild(newProjectBox);
}

function loadProjects() {
    fetch("/projects")
            .then(response => response.json())
            .then(json => json.data)
            .then((data) => {
                const doc = document;
                doc.getElementById("projectsDesc").innerText = data.secondaryTitle;
                doc.getElementById("projectsMainText").innerText = data.pageDesc;
                let projectsNr = data.projects.length,
                    projectsContainer = doc.getElementById("projects"),
                    nextProjects = doc.createElement("a"),
                    prevProjects = doc.createElement("a");

                nextProjects.id = "nextProjects";
                nextProjects.innerText = ">";
                nextProjects.addEventListener('click', slideProjects);

                prevProjects.id = "prevProjects";
                prevProjects.innerText = "<";
                prevProjects.addEventListener('click', slideProjects);

                projectsContainer.appendChild(prevProjects);
                for (let i in data.projects) {
                    createProjectBox(doc, data.projects[i], projectsContainer);
                }
                projectsContainer.appendChild(nextProjects);

                let projects = document.getElementsByClassName("projectBox"),
                    projectsHeight = document.getElementById("projectsPage");

                if (screen.width > screen.height) {
                    maxProjects = 3;
                } else {
                    maxProjects = projectsNr;
                    projectsHeight.style.height = 50 + projectsNr * 55 + "vh";
                    projectsContainer.style.height = projectsNr * 50 + "vh";
                }

                if (projectsNr > maxProjects) {
                    for (let i = maxProjects; i < projectsNr; i++) {
                        projects[i].classList.replace("visibleProject", "remove");
                    }
                } else {
                    nextProjects.classList.add("makeInvisible");
                }
                rightmostProject = projectsNr < 3 ? projectsNr - 1 : 2;
                leftmostProject = 0;
                prevProjects.classList.add("makeInvisible");
            })
            .catch("Could not fetch /projects");
}

function slideProjects(event) {
    let projects = document.getElementsByClassName("projectBox"),
        projectsNr = projects.length,
        projectNext = document.getElementById("nextProjects"),
        projectPrev = document.getElementById("prevProjects");
    if (event.target.id === "nextProjects" && rightmostProject < projectsNr - 1) {
        slideNr = (projectsNr - rightmostProject - 1) / maxProjects >= 1 ? maxProjects : (projectsNr - rightmostProject - 1) % maxProjects;
        for (let i = 0; i < slideNr; i++) {
            projects[leftmostProject].classList.replace("visibleProject", "remove");
            leftmostProject++;
            rightmostProject++;
            projects[rightmostProject].classList.replace("remove", "visibleProject");
        }
    } else if (event.target.id === "prevProjects" && leftmostProject > 0) {
        slideNr = leftmostProject / maxProjects >= 1 ? maxProjects : leftmostProject % maxProjects;
        for (let i = 0; i < slideNr; i++) {
            projects[rightmostProject].classList.replace("visibleProject", "remove");
            leftmostProject--;
            rightmostProject--;
            projects[leftmostProject].classList.replace("remove", "visibleProject");
        }
    }
    if (rightmostProject === projectsNr - 1 && projectNext.classList.contains("makeInvisible") === false) {
        projectNext.classList.add("makeInvisible");
    } else if (projectNext.classList.contains("makeInvisible")) {
        projectNext.classList.remove("makeInvisible");
    }
    if (leftmostProject === 0 && projectPrev.classList.contains("makeInvisible") === false) {
        projectPrev.classList.add("makeInvisible");
    } else if (projectPrev.classList.contains("makeInvisible")) {
        projectPrev.classList.remove("makeInvisible");
    }
}